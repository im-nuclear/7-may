﻿using System;

namespace _Roy
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Random rnd = new Random();
            int firstNum = rnd.Next(1, 20);
            int secondNum = rnd.Next(50, 100);


            Console.WriteLine("Hello!");
            Console.ReadLine();
            Console.WriteLine("Roy Here!");
            Console.ReadLine();
            Console.WriteLine("This is to refresh my mind on C#");
            Console.ReadLine();
            Console.WriteLine(firstNum + secondNum);
            Console.ReadLine();

            Console.Write("Now enter a big number: ");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Now enter a small number: ");
            int num2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Bleep Bloop Boop Bleep");
            Console.ReadLine();
            Console.WriteLine(num1 + num2);
            Console.WriteLine(num1 - num2);
            Console.WriteLine(num1 * num2);
            Console.WriteLine(num1 / num2);
            Console.WriteLine(num1 % num2 + "\n");
            Console.ReadLine();
            Console.WriteLine("Now lets clear this mess we made");
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine("Now we loop 5 times");
            Console.Write("Press enter to continue\n");
            Console.ReadLine();

            for (int num = 1; num <= 5; num++)
            {
                Console.WriteLine("For Loop " + num);
            }

            Console.WriteLine("Yep we did it");
            Console.ReadLine();
            Console.WriteLine("Thats all for this program/nGoodbye!");
            Console.ReadLine();
            Console.Clear();
        }
    }
}
